@extends('store.storeLayout')
<script src="https://test.oppwa.com/v1/paymentWidgets.js?checkoutId={{$id}}"></script>
@section('content')

<!-- SECTION -->
<div class="section">
     <form action="{{ route('Checkout_response',$id)}}" class="paymentWidgets" data-brands="VISA MASTER AMEX PAYPAL"></form>
</div>
@endsection
<!-- /SECTION -->
