    @extends('store.storeLayout')
@section('content')
<script src="{{asset('js/lib/jquery.js')}}"></script>
<script src="{{asset('js/dist/jquery.validate.js')}}"></script>
 <script data-require="jquery@3.1.1" data-semver="3.1.1" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<link type="text/css" rel="stylesheet" href="{{asset('css/style_for_quantity.css')}}" />

<style>
label.error {
  color: #a94442;
  background-color: #f2dede;
  border-color: #ebccd1;
  padding:1px 20px 1px 20px;
}
.product-preview img {
    border: 3px solid #ad9802;
    border-radius: 10px;
}
.product-details .product-rating>i.fa-star{
    color: #ad9802;
    -webkit-text-stroke: 1px black;
}
.product-details .product-rating>i {
    -webkit-text-stroke: 1px black;
}
.product-available{
    color: #ad9802;
}
.row{
    margin: 0px 0px 5px 0px;
}
.product-details .product-name {
    text-transform: uppercase;
    font-size: 40px;
    margin-top: 30px;
}
.question{
    border-radius: 40px; background-color: gray; color: white;
}

.question:focus {
  background-color: #ad9802;
}
#quantity{
    border-radius: 40px;
}
input, select{
 
    -ms-box-sizing:content-box;
    -moz-box-sizing:content-box;
    box-sizing:content-box;
    -webkit-box-sizing:content-box; 
    
}
.add-to-cart .add-to-cart-btn {
    position: relative;
    border: 2px solid transparent;
    height: 40px;
    padding: 0 30px;
    background-color:#ad9802;
    color: #FFF;
    text-transform: uppercase;
    font-weight: 700;
    border-radius: 40px;
    -webkit-transition: 0.2s all;
    transition: 0.2s all;
    margin-bottom: 30px;}
    label{
        text-align: left;
    }
    .add-to-cart-btn{
        margin: 10px 0px 10px 0px;
    }
   
</style>

<!-- SECTION -->
<div class="section">
    <!-- container -->
    <div class="container">
        <!-- row -->
        <div class="row">
            <!-- Product main img -->
            <div class="col-md-6 " style="padding: 15px 15px 15px">
                <div id="product-main-img">
                    <div class="product-preview">
                        <img src="../uploads/products/{{$product->id}}/{{$product->image_name}}" alt="">
                    </div>
                </div>
            </div>
            <!-- /Product main img -->


            <!-- Product details -->
            <div class="col-md-6">
                <div class="product-details">
                    <h1 class="product-name">{{$product->name}}</h1>
                    <ul class="product-links">
                        <li><a href="{{route('user.search')}}?c={{$product->category->id}}">{{$product->category->name}}</a></li>
                    </ul>
                    <div>
                        <div class="product-rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-o"></i>
                        </div>
                    </div>
                    <div>
                        <h3 class="product-price">£ {{$product->discount}} <del class="product-old-price" hidden>£ {{$product->price}}</del></h3>
                        <span class="product-available">In Stock</span>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                        <p>{!!$product->description!!}</p>
                        </div>
                    </div>
                    
                    <form method="post" id="order_form" action="{{route('user.postcart',$product->id)}}">
                    {{csrf_field()}}
                    <div class="product-options row" >
                        <input type="hidden" id="discount_price_holder" name="discount_price_holder" value={{$product->discount}}>
                        <label>
                        
                        
                       
                    </div>
                    
                        
                        
                        @foreach($colors as $c)
                        <input type="radio" name="color" hidden checked="checked value="#00000">
                        <div style="height:25px;width:25px;margin:5px;display:inline-block;background-color: #000000" hidden></div>
                        @endforeach
                          
                    </div>
                   		<span hidden="hidden">{{$Q = 0}}</span>
                        @foreach($questions as $q)
                        <div class="row" >
                            <label class="col-md-6" > {{ $question = \App\Question::where(['id' => $q->question_id])->value('question')}}</label>
                            <label hidden="hidden" > {{$options = \App\Option::where(['question_id' => $q->question_id])->get()}}</label>
                            <input type="text" name="tq" value="{{$Q = $Q+1}}" hidden="hidden">
                            </div>
                            <div class="row">
                            <input type="text" name="questions[]" value="{{$question}}" hidden="hidden">
                            <select class="form-control col-md-10 question" name="questionA[]" style="">
                            @foreach($options as $o)
                            <option>
                                {{$o->option}}
                            </option>
                            @endforeach
                            </select>
                        </div>
                       @endforeach
                 
                        <div id="for_error"></div>

                    <div class="add-to-cart row" style="margin-top: 30px">
                       
                        <label class="col-md-2" style="font-weight: bold; margin-top: 15px">QUANTITY:</label>
                       
                   
                        <input type="number" id="quantity" inputmode="numeric" name="quantity" class="quantity form-control col-md-3" value="1" style="margin-right: 60px;"  />
                   
                        
                        <button type="submit" name="myButton" id="myButton" class=" add-to-cart-btn col-md-4"><i class="fa fa-shopping-cart"></i> add to cart</button>
                         
                    </div>
                    </form>
                    
                </div>
            </div>
            <!-- /Product details -->

        </div>
        <!-- /row -->
    </div>
    <!-- /container -->
</div>
<div style="height:200px"></div>

<!--JQUERY Validation-->
<script>
	
    //////////////////////////////////////
    $(document).ready(function() {
		
		$("#order_form").validate({
			
            submitHandler: function (form) {
            if($('input[name=color]:checked').val()==undefined)
            {
                
            document.getElementById("for_error").innerHTML = "<label class='error' style=' '>Invalid Variation Input</label>";

            }
                else
                    {
                        return true;
                    }
                
         }
		});

		
	});
	
    $('.add').click(function () {
        
        $(this).prev().val(+$(this).prev().val() + 1);
        
    });
    $('.sub').click(function () {
            if ($(this).next().val() > 1) {
            $(this).next().val(+$(this).next().val() - 1);
            }
    });
    
	
   
	</script>
<!--/JQUERY Validation-->
<!-- /SECTION -->
@endsection
