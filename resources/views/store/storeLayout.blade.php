<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>The Halal Butchery</title>

    <link rel="shortcut icon" href="{{asset('img/THB-fav.png')}}" />
    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">

    <!-- Bootstrap -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}" />


    <!-- Slick -->
    <link type="text/css" rel="stylesheet" href="{{asset('css/slick.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('css/slick-theme.css')}}" />

    <!-- nouislider -->
    <link type="text/css" rel="stylesheet" href="{{asset('css/nouislider.min.css')}}" />

    <!-- Font Awesome Icon -->
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">

    <!-- Custom stlylesheet -->
    <link type="text/css" rel="stylesheet" href="{{asset('css/style2.css')}}" />
    
    <!-- JQuery and Validator Plugins -->
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    {{-- custom css --}}
    <style>
        @media only screen and (max-width: 767px){
            #head_links {
                visibility: hidden;
            }
            .custom_search_top {
                text-align:center;
            }

            .header-ctn {
                width: 100%;
            }
        }
        .img-size{
            max-height: 275px;
            min-height: 275px; 
        }
        .carousel-item h3{
            color: white;
        }
         .carousel-inner img {
            width: 100%;
            height: 100%;
        }
        .container{
            min-width: 100%;
        }
        .shop:before {
            width: 45%;
        }
        .carousel-item img{
            border-bottom: 3px solid #ad9802;
            border-top: 3px solid #ad9802;
            border-left: 2px solid #ad9802;
            border-right: 2px solid #ad9802;

        }
        .product-preview img{
            border: 3px solid #D10024;
        }
        .quantity
        {
            height: 35px;
            width: 45px;
            border-radius: 10px;
            text-align: center;
            font-weight: bold;
            background-color: gray;
            color:white;
            font-size: 15px;

        }
        .quantity:focus{
            background-color: #ad9802;
        }
        .product{
        	border-radius: 10px;
            border: 1px solid #ad9802; 
        }
        .product-label{
        	border-radius: 10px;
        } 
        .product-body{
        	border-radius: 10px;
        } 
        .add-to-cart{
        	border-radius: 10px;
        }
        .img-size
        {
        	border-top-left-radius: 10px;
    		border-top-right-radius: 10px;
        }
        .section .container .row{
        	    
        	    text-align: center;
        }
        .active{
        	color: red;
        	color: #ad9802;
        	font-weight: bold;
        }
        button
        {
        	background-color: #ad9802;
        }
        #navigation {
        	border-top: 3px solid #ad9802;
        }
        .product .add-to-cart .add-to-cart-btn {
            background-color: #ad9802;
        }
        input{
            border-radius: 50px;
            background: gray;
        }
        .badge {
  padding-left: 9px;
  padding-right: 9px;
  -webkit-border-radius: 9px;
  -moz-border-radius: 9px;
  border-radius: 9px;
}

.label-warning[href],
.badge-warning[href] {
  background-color: #c67605;
}
#lblCartCount {
    font-size: 12px;
    background: #961515;
    color: #fff;
    padding: 1px 3px;
    vertical-align: top;
    margin-left: -5px; 
}
.steps{
    background: black;
    border: 2px solid #ad9802;
}
.steps img{
    display: block;
    margin-left: auto;
    margin-right: auto;
}
        </style>

</head>

<body>
    <!-- HEADER -->
    <header>
        <!-- TOP HEADER -->
        <div id="top-header">
            <div class="container" style="min-width: 90%">
                <ul id="head_links" class="header-links pull-left">
                    <li><a href="#"><i class="fa fa-envelope-o" style="color: #ad9802"></i> hello@thehalalbutchery.com</a></li>
                   
                    <li style="border-left: 1px solid #ad9802; padding-left: 10px; "><a href="#"> About Us</a></li>

                    <li style="border-left: 1px solid #ad9802; padding-left: 10px; "><a href="#"> Delivery Process</a></li>
                </ul>
                <ul class="header-links pull-right">
                    @if(session()->has('user'))
                      <li><a style="color:white" href="{{route('user.history')}}">{{session()->get('user')->full_name}} </a></li>  
                      <li><a href="{{route('user.logout')}}"><i class="fa fa-user-o" style="color: #ad9802"></i> Logout</a></li>
                    @else
                    <li><a href="{{route('user.login')}}"><i class="fa fa-user-o" style="color: #ad9802"></i> Login</a></li>
                    
                    <li><a href="{{route('user.signup')}}"><i class="fa fa-user-o" style="color: #ad9802"></i> SignUp</a></li>
                    @endif
                    
                </ul>
            </div>
        </div>
        <!-- /TOP HEADER -->

        <!-- MAIN HEADER -->
        <div id="header">
            <!-- container -->
            <div class="container" style="min-width: 90%">
                <!-- row -->
                <div class="row">
                    <!-- LOGO -->
                    <div class="col-md-3">
                        <div class="header-logo">
                            <a href="{{route('user.home')}}" class="logo">
                                <img src="{{asset('img/THB-Logo.png')}}" alt="">
                            </a>
                        </div>
                    </div>
                    <!-- /LOGO -->

                    <!-- SEARCH BAR -->
                    <div class="col-md-6" style="margin-top: 30px">
                        <div class="header-search">
                            <form action="{{route('user.search')}}" method="get">
                                <div class="custom_search_top" >
                                    <input class="input" style="border-radius: 40px 0px 0px 40px;" name="n" placeholder="Search here">
                                    <button  class="search-btn" style="background-color: #ad9802">Search</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- /SEARCH BAR -->

                    <!-- ACCOUNT -->
                    <div class="col-md-3 clearfix"  style="margin-top: 30px">
                        <div class="header-ctn">
                            <!-- Cart -->
                            <div  class="dropdown">
                                <a class="dropdown-toggle " id="custom_shopping_cart" href="{{route('user.cart')}}">
                                    <i class="fa fa-shopping-cart" matBadge="15" style="color: #ad9802"><span class='badge badge-warning' id='lblCartCount'> {{ (Session::get('orderCounter'))?Session::get('orderCounter'):"0"}} </span></i>
                                    <span >Your Cart</span>
                                </a>

                            </div>
                            <!-- /Cart -->

                            <!-- Menu Toogle -->
                            <div class="menu-toggle pull-right" >
                                <a href="#">
                                    <i class="fa fa-bars"></i>
                                    <span>Menu</span>
                                </a>
                            </div>
                            <!-- /Menu Toogle -->
                        </div>
                    </div>
                    <!-- /ACCOUNT -->
                </div>
                <!-- row -->
            </div>
            <!-- container -->
        </div>
        <!-- /MAIN HEADER -->
    </header>
    <!-- /HEADER -->

    <!-- NAVIGATION -->
    <nav id="navigation">
        <!-- container -->
        <div class="container" style="min-width: 90%">
            <!-- responsive-nav -->
            <div id="responsive-nav">
                <!-- NAV -->
                <ul class="navbar" >
                    <li ><a href="{{route('user.home')}}" class="{{Route::is('user.home') ? 'active' : ''}}">Home</a></li>
                    @if(Route::is('user.search'))
                        @foreach($cat as $c)
                        <li ><a href="{{route('user.search.cat',['id'=>$c->id])}}" class=" nav-item {{$c->id == $a ? 'active' : ''}}">{{$c->name}}</a></li>
                        @endforeach
                        <li ><a href="search" class=" nav-item {{$a == -1  ? 'active' : ''}}">Browse All</a></li>
                    @else
                        @foreach($cat as $c)
                        <li ><a href="{{route('user.search.cat',['id'=>$c->id])}}" >{{$c->name}}</a></li>
                        @endforeach
                        <li ><a href="{{route('user.search')}}">Browse All</a></li>
                    @endif
                    
                </ul>
                <!-- /NAV -->
            </div>
            <!-- /responsive-nav -->
        </div>
        <!-- /container -->
    </nav>
    <!-- /NAVIGATION -->

    <!-- SECTION -->
    <div class="alert alert-success" hidden role="alert">
        Your order Succesfully Placed!
    </div>
    <div class="section">
        <!-- container -->
        <div class="container" style="padding: 0px;">
            <!-- row -->
            @if(Route::is('user.home'))
            <!-- Slider Stat -->
            <div id="demo" class="carousel slide" data-ride="carousel">
              <ul class="carousel-indicators">
                <li data-target="#demo" data-slide-to="0" class="active"></li>
                <li data-target="#demo" data-slide-to="1"></li>
                <li data-target="#demo" data-slide-to="2"></li>
              </ul>
              <div class="carousel-inner">
                <div class="carousel-item active">
                  <img src="{{asset('images/slides/1.jpg')}}" alt="Los Angeles" width="100%" height="500">
                  <div class="carousel-caption">
                    <h3>Marinated Meat</h3>
                    <p>THB have a range of marinated meat!</p>
                  </div>   
                </div>
                <div class="carousel-item">
                  <img src="{{asset('images/slides/2.jpg')}}" alt="Chicago" width="100%" height="500">
                  <div class="carousel-caption">
                    <h3>Lamb</h3>
                    <p>Lamb meat is on Promotion!</p>
                  </div>   
                </div>
                <div class="carousel-item">
                  <img src="{{asset('images/slides/3.jpg')}}" alt="New York" width="100%" height="500">
                  <div class="carousel-caption">
                    <h3>Beef Ribs</h3>
                    <p>TBH offering Beef Ribs on 20% off for new customers!</p>
                  </div>   
                </div>
              </div>
              <a class="carousel-control-prev" href="#demo" data-slide="prev">
                <span class="carousel-control-prev-icon"></span>
              </a>
              <a class="carousel-control-next" href="#demo" data-slide="next">
                <span class="carousel-control-next-icon"></span>
              </a>
            </div>

            <!-- Slider End -->
            <div class="row" style="padding: 15px; background: none ">
                <!-- shop -->
                @php
                $counter=0;
                @endphp
                @foreach($cat as $c)
                 @php
                $counter++;
                if($counter==4)
                break;
               
                @endphp
                <div class="col-md-4 col-xs-6">
                    <div class="shop">
                        <div class="shop-img">
                            <img class="img-size" src="./img/shop0{{$index++}}.png" alt="">
                        </div>
                        <div class="shop-body">
                            <h3>{{$c->name}}</h3>
                            <a href="search?c={{$c->id}}" class="cta-btn">Shop now <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                </div>
                <!-- /shop -->
                @endforeach
            </div>
            @endif
            <!-- /row -->


        </div>
        <!-- /container -->
    </div>
    <!-- SECTION -->
    
    <!-- Section -->
            <div class="section steps">
                <div class="container">
                    <img src="{{asset('img/THB_Steps.png')}}" alt="">
                </div>
            </div>
    <!-- Section -->


    @yield('content')

    <!-- /SECTION -->
    
    <div id="newsletter" class="section">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="newsletter">
                        <p>Sign Up for the <strong>NEWSLETTER</strong></p>
                        <form>
                            <input class="input" type="email" placeholder="Enter Your Email">
                            <button class="newsletter-btn"><i class="fa fa-envelope"></i> Subscribe</button>
                        </form>
                        <ul class="newsletter-follow">
                            <li>
                                <a href="#"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-instagram"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-pinterest"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /NEWSLETTER -->

    <!-- FOOTER -->
    <footer id="footer" >
        <!-- top footer -->
        <div class="section">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row" >
                    <div class="col-md-3 col-xs-6" >
                        <div class="footer" >
                            <h3 class="footer-title">About Us</h3>
                            <p>We deal in Fresh meat.</p>
                            <ul class="footer-links">
                                <li><a href="#"><i class="fa fa-map-marker"></i>Chorltan</a></li>
                                <li><a href="#"><i class="fa fa-phone"></i>+44-11-22-33</a></li>
                                <li><a href="#"><i class="fa fa-envelope-o"></i>info@thehalalbutchery.com</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-3 col-xs-6">
                        <div class="footer">
                            <h3 class="footer-title">Categories</h3>
                            <ul class="footer-links">
                                <li><a href="#">Hot deals</a></li>
                                <li><a href="#">Chicken</a></li>
                                <li><a href="#">Beef</a></li>
                                <li><a href="#">Mutton</a></li>
                                <li><a href="#">Lamb</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="clearfix visible-xs"></div>

                    <div class="col-md-3 col-xs-6">
                        <div class="footer">
                            <h3 class="footer-title">Information</h3>
                            <ul class="footer-links">
                                <li><a href="#">About Us</a></li>
                                <li><a href="#">Contact Us</a></li>
                                <li><a href="#">Privacy Policy</a></li>
                                <li><a href="#">Orders and Returns</a></li>
                                <li><a href="#">Terms & Conditions</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-3 col-xs-6">
                        <div class="footer">
                            <h3 class="footer-title">Service</h3>
                            <ul class="footer-links">
                                <li><a href="#">My Account</a></li>
                                <li><a href="#">View Cart</a></li>
                                <li><a href="#">Wishlist</a></li>
                                <li><a href="#">Track My Order</a></li>
                                <li><a href="#">Help</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /top footer -->

        <!-- bottom footer -->
        <div id="bottom-footer" class="section">
            <div class="container">
                <!-- row -->
                <div class="row">
                    <div class="col-md-12 text-center">
                        <ul class="footer-payments">
                            <li><a href="#"><i class="fa fa-cc-visa"></i></a></li>
                            <li><a href="#"><i class="fa fa-credit-card"></i></a></li>
                            <li><a href="#"><i class="fa fa-cc-paypal"></i></a></li>
                            <li><a href="#"><i class="fa fa-cc-mastercard"></i></a></li>
                            <li><a href="#"><i class="fa fa-cc-discover"></i></a></li>
                            <li><a href="#"><i class="fa fa-cc-amex"></i></a></li>
                        </ul>
                    </div>
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /bottom footer -->
    </footer>
    <!-- /FOOTER -->


    <!-- jQuery Plugins -->
   <!--  <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="{{asset('js/slick.min.js')}}"></script>
    <script src="{{asset('js/nouislider.min.js')}}"></script>
    <script src="{{asset('js/jquery.zoom.min.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
    <script src="{{asset('js/lib/jquery.js')}}"></script>
    <script src="{{asset('js/dist/jquery.validate.js')}}"></script>
    
    

</body>

</html>
