@extends('admin_panel.adminLayout') @section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Admins Table <a class="btn btn-lg btn-success" style="float:right;color:white" href="{{route('admin.adminCreate')}}">+ Add Admin</a></h4>
                    <br><br>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>
                                        User Name
                                    </th>
                                    <th>
                                        Name
                                    </th>
                                    <th>
                                        Postcode
                                    </th>
                                    <th>
                                        Roles
                                    </th>
                                    <th>
                                        Status
                                    </th>
                                    <th>
                                        Action
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($admins as $adm)
                                <tr>
                                    <td>
                                        {{$adm->username}}
                                    </td>
                                    <td>
                                        {{$adm->name}}
                                    </td>
                                    <td>
                                        {{$adm->postcode}}
                                    </td>
                                    <td>
                                        {{ (implode(',',$adm->roles()->get()->pluck('name')->toArray())) }}
                                    </td>
                                    <td>
                                        {{ ($adm['status'])? "Active":"Deactive" }}
                                    </td>
                                    <td>
                                        <a href="{{route('admin.adminEdit',$adm->id)}}">Edit</a>
                                    </td>
                                    
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

