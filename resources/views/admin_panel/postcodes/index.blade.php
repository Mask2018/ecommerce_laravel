@extends('admin_panel.adminLayout') @section('content')
<script src="{{asset('js/lib/jquery.js')}}"></script>
<script src="{{asset('js/dist/jquery.validate.js')}}"></script>
<style>label.error {
  color: #a94442;
  background-color: #f2dede;
  border-color: #ebccd1;
  padding:1px 20px 1px 20px;
}</style>



<div class="content-wrapper">
    <div class="row">
        <div class="col-12 stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Add Postcode</h4>
                    <form class="forms-sample" method="post" id="postcode_form" action="{{route('Postcode.PostcodeStore')}}">
                        {{csrf_field()}}
                        <div class="form-group row">
                            <label for="exampleInputEmail2" class="col-sm-3 col-form-label">Postcode</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="postcode" id="postcode" placeholder="Enter Postcode">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Area</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="area" id="area" placeholder="Enter Area">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="exampleInputPassword2" class="col-sm-3 col-form-label">City</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="city" id="city" value="Manchester" placeholder="Enter City" readonly>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success mr-2">Submit</button>
                    </form>
                    @if($errors->any())
                    <ul>
                        @foreach($errors->all() as $err)
                        <tr>
                            <td>
                                <li>{{$err}}</li>
                            </td>
                        </tr>
                        @endforeach
                    </ul>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Categories Table</h4>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>
                                        Postcode
                                    </th>
                                    <th>
                                        Area
                                    </th>
                                    <th>
                                        City
                                    </th>
                                    <th>
                                        Status
                                    </th>
                                    <th>Assigned To</th>
                                    <th>
                                        Edit
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($postcodelist as $cat)
                                <tr>
                                    <td>
                                        {{$cat->postcode}}
                                    </td>
                                    <td>
                                        {{$cat->area}}
                                    </td>
                                    <td>
                                        {{$cat->city}}
                                    </td>
                                    <td>
                                        {{ ($cat->status==1)?"Assigned":"Free"}}
                                    </td>
                                    <td>{{  (implode(',',$cat->admins()->get()->pluck('name')->toArray())) }}</td>
                                    <td>
                                        <a href="{{route('Postcode.PostcodeEdit', ['id' => $cat->id])}}" class="btn btn-warning">Edit</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!--JQUERY Validation-->
<script>
	
	$(document).ready(function() {
		
		$("#cat_form").validate({
			rules: {
				Name: "required",
				Type: "required",
				
				
				
			},
			messages: {
				Name: "Category Name is Required",
				Type: "Category Type is Required",
                	
			}
		});

		
	});
	</script>
<!--/JQUERY Validation-->

@endsection
